$(function(){

	/*****************************************
	*           CARROSSEIS                   *
	*****************************************/

	$('.carrossel-destaque').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		fade: true,
		dots: true,
		infinite: true,
		speed: 450,
		autoplay: true,
		autoplaySpeed: 3000,
		centerMode: true,
		pauseOnHover: true
	});

	$('.carrossel').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: true,
		fade: false,
		dots: false,
		infinite: false,
		speed: 450,
		autoplay: false,
		autoplaySpeed: 3000,
		centerMode: false,
		pauseOnHover: true,
		edgeFriction: 0,
		responsive: [
	       	{
	          breakpoint: 400,
	          settings: {
	            slidesToShow: 1,
	           
	          }
	        },
			{
	          breakpoint: 610,
	          settings: {
	            slidesToShow: 2,
	           
	          }
	        },
	        {
	          breakpoint: 810,
	          settings: {
	            slidesToShow: 3,
	           
	          }
	        },
	        {
	          breakpoint: 991,
	          settings: {
	            slidesToShow: 4,
	          }
	        }
	      ]
	});

	$('.carrossel-produto').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: true,
		fade: false,
		dots: false,
		infinite: true,
		speed: 450,
		autoplay: false,
		autoplaySpeed: 3000,
		centerMode: false,
		pauseOnHover: true,
		edgeFriction: 0
	});

	$('.pg-produto ul.carrossel-produto li.item-produto figure img').click(function(){
		$('.pg-produto figure.imagem-produto img').attr('src', $(this).attr('src'));
	});

	let isOpen = false;
	$('header .abrir-menu-mobile').click(function(){
		$('header .menu-principal').toggleClass('menu-mobile-ativo');

		if(!isOpen){
			$('header .abrir-menu-mobile span.um,header .abrir-menu-mobile span.tres').addClass('translate');
			$('header .abrir-menu-mobile span.dois').addClass('opacity-zero');
			setTimeout(function(){
				$('header .abrir-menu-mobile span.um,header .abrir-menu-mobile span.tres').addClass('rotate');
			},200);
			isOpen = true;
		} else{
			$('header .abrir-menu-mobile span.um,header .abrir-menu-mobile span.tres').removeClass('rotate');
			$('header .abrir-menu-mobile span.dois').removeClass('opacity-zero');
			setTimeout(function(){
				$('header .abrir-menu-mobile span.um,header .abrir-menu-mobile span.tres').removeClass('translate');
			},200);
			isOpen = false;
		}
	});

	$('.pg ul.lista-produtos li.item-produto a.imagem-produto figure').mouseover(function(){
		$(this).children().attr('src', $(this).children().attr('data-hover'));
	});

	$('.pg ul.lista-produtos li.item-produto a.imagem-produto figure').mouseout(function(){
		$(this).children().attr('src', $(this).children().attr('data-inicial'));
	});

});